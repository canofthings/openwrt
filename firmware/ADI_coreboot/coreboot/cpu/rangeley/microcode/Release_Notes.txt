Combined Patch Release Notes
*Only apply the patch which applies.
Product			Microcode Patch
Avoton A1 Stepping	M01406D000E
Avoton B0 Stepping	M01406D8121

patch	change description
121
		4686823 PECI MaxPkgPowerOfSKU is off by 1
		4686797 DTS is out of range for temps below -27C
		4686779 Turbo not functional after a significantly large number of warm resets.
		4686814 Gbe not functional after a random number of resets because of EEPROM load error
11f
		4686789 Adds PMC FW Watchtog timeout configuration via soft straps using SOFT_STRAP_4 bit 2

11E
		All C6 request demoted to C6NS
		RTIT flow problem with C-State transitions
		4686792 System hangs when changing the number of active cores
		4686788 wrong CPU ID data returned by PECI rdpkgcfg command
		4686592 Locks ups/execution stops during Disk I/O Benchmarks
		Addition of TDP MSR  0x66e